const API = 'http://localhost:8080';

const users = [];
async function addUser(userObject){
    axios.post(`${API}/api/users`, userObject)
        .then(response => {
            console.log(response);
        })
        .catch(error => {
            console.log(error);
        })
}

export {
    API,
    users,
    addUser
}
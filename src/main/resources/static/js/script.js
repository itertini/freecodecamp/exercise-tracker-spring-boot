// import { API } from './store'

const API = 'http://localhost:8080';

const userForm = document.querySelector("#userForm");
const userTable = document.querySelector('#userTable');
const exerciseForm = document.querySelector('#exerciseForm');
const modal = document.querySelector('#modal');

let users = [];

function render() {
    userTable.innerHTML = '';

    for (let i = 0; i < users.length; i++) {
        let userRow = document.createElement('tr');
        userRow.innerHTML = `
            <td>${users[i].userId}</td>
            <td>${users[i].username}</td>
            <td><button id="toggle-modal-${i}" class="btn btn-sm btn-outline-primary">Show Exercises</button></td>
            <td><i id="deleteUserButton-${i}" class="fa-solid fa-trash deleteButton"></i></td>
        `
        userTable.append(userRow);

        document.querySelector('#toggle-modal-' + i).addEventListener('click', () => {
            renderModal(users[i].userId)
        })

        document.querySelector('#deleteUserButton-' + i).addEventListener('click', () => {
            axios.delete(`${API}/api/users/${users[i].userId}`)
            users = users.filter(user => user.userId !== users[i].userId)
            render();
        })
    }
}

function renderModal(id) {
    let user = null;
    const userId = document.querySelector('#modal-userid');
    const username = document.querySelector('#modal-username');
    const count = document.querySelector('#modal-usercount');
    const logs = document.querySelector("#modal-logs")

    axios.get(`${API}/api/users/${id}/logs`)
        .then(response => {
            user = response.data;
            userId.innerText = user?.userId;
            username.innerText = user?.username;
            count.innerText = user?.count;

            logs.innerHTML = ''

            user.logs.forEach(exercise => {
                console.log(exercise)
                let exerciseEntry = document.createElement('tr');
                exerciseEntry.innerHTML = `
                    <td>${exercise.description}</td>
                    <td>${exercise.duration}</td>
                    <td>${exercise.date}</td>
                    <td><i id="deleteExerciseButton" class="fa-solid fa-trash deleteButton"></i></td>
                `
                logs.append(exerciseEntry);

                exerciseEntry?.querySelector('#deleteExerciseButton').addEventListener('click', () => {
                    console.log(exercise.exerciseId)
                    axios.delete(`${API}/api/users/exercises/${exercise.exerciseId}`)
                })
            })
            toggleModal();
        })
        .catch(error => {
            console.log(error);
        })
}

function toggleModal(){
    console.log("toggleModal")
    document.querySelector('#modal').classList.toggle('modal-visible')
}

// create User
userForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const username = document.querySelector('#usernameInput');
    const userObject = {
        username: username.value
    };
    username.value = '';

    await axios.post(`${API}/api/users`, userObject)
        .then(response => {
            users.push(response.data);
            render();
        })
        .catch(error => {
            console.log(error.code);
        })
})

// Add exercise
exerciseForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const id = document.querySelector('#idInput');
    const description = document.querySelector('#descriptionInput');
    const duration = document.querySelector('#durationsInput');
    const date = document.querySelector('#dateInput');

    const exerciseObject = {
        description: description.value,
        duration: duration.value,
        date: date.value
    }
    console.log(id.value)
    await axios.post(`${API}/api/users/${id.value}/exercises`, exerciseObject)
        .then(response => {
            console.log(response.data)

        })
        .catch(error => {
            console.log(error.code);
        })
    id.value = '';
    description.value = '';
    duration.value = '';
    date.value = '';

})

modal.addEventListener('click', () => {
    toggleModal()
})

window.addEventListener('load',async () => {
    if (users.length === 0) {
        await axios.get(`${API}/api/users`)
            .then(response => {
                users = response.data

            })
            .catch(error => {
                console.log(error.code);
            })
    }
    render();
})
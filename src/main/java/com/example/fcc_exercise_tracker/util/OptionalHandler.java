package com.example.fcc_exercise_tracker.util;

import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class OptionalHandler {

    public <T> T getFromOptional(Optional<T> optional) {
        if (optional.isEmpty()) {
            throw new NoSuchElementException();
        }
        return optional.get();
    }
}

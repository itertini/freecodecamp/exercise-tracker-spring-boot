package com.example.fcc_exercise_tracker.entity;

import jakarta.persistence.*;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long exerciseId;

    private String description;

    private int duration;

    private String date;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;
}

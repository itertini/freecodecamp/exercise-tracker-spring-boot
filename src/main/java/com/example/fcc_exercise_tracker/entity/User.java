package com.example.fcc_exercise_tracker.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;

    @Column(unique = true)
    private String username;

    private int count;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Exercise> logs;
}

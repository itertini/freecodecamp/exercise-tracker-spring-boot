package com.example.fcc_exercise_tracker.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ResponseUserDTO {

    private long userId;

    private String username;
}

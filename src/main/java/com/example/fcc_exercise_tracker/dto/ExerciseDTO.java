package com.example.fcc_exercise_tracker.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ExerciseDTO {

    private long exerciseId;

    private String description;

    private int duration;

    private String date;
}

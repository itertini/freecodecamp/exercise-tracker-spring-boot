package com.example.fcc_exercise_tracker.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CreateUserDTO {

    private String username;
}

package com.example.fcc_exercise_tracker.dto;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class FullResponseUserDTO {

    private long userId;

    private String username;

    private int count;

    private List<ExerciseDTO> logs;
}

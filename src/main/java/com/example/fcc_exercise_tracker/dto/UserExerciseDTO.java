package com.example.fcc_exercise_tracker.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserExerciseDTO {

    private long userId;

    private String username;

    private String description;

    private int duration;

    private String date;
}

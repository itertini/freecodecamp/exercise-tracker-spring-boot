package com.example.fcc_exercise_tracker.service;

import com.example.fcc_exercise_tracker.dto.CreateUserDTO;
import com.example.fcc_exercise_tracker.dto.ResponseUserDTO;
import com.example.fcc_exercise_tracker.entity.User;
import com.example.fcc_exercise_tracker.repository.ExerciseCrudRepository;
import com.example.fcc_exercise_tracker.repository.UserCrudRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserCrudRepository userCrudRepository;

    private final ExerciseCrudRepository exerciseCrudRepository;

    public ResponseUserDTO createUser(CreateUserDTO createUserDTO) {
        User user = User.builder()
                .username(createUserDTO.getUsername())
                .count(0)
                .build();
        userCrudRepository.save(user);
        return ResponseUserDTO.builder()
                .userId(user.getUserId())
                .username(user.getUsername())
                .build();
    }

    public List<ResponseUserDTO> getAllUsers() {
        List<ResponseUserDTO> users = new ArrayList<>();
        userCrudRepository.findAll().forEach(user -> {
            int count = exerciseCrudRepository.getExcerciseCount(user) == null ? 0 : exerciseCrudRepository.getExcerciseCount(user);
            users.add(ResponseUserDTO.builder()
                            .userId(user.getUserId())
                            .username(user.getUsername())
                            .build());
        });
        return users;
    }

    public void deleteUser(long userId) {
        userCrudRepository.deleteById(userId);
    }
}

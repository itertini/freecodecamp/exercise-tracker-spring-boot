package com.example.fcc_exercise_tracker.service;

import com.example.fcc_exercise_tracker.dto.ExerciseDTO;
import com.example.fcc_exercise_tracker.dto.FullResponseUserDTO;
import com.example.fcc_exercise_tracker.dto.UserExerciseDTO;
import com.example.fcc_exercise_tracker.entity.Exercise;
import com.example.fcc_exercise_tracker.entity.User;
import com.example.fcc_exercise_tracker.repository.ExerciseCrudRepository;
import com.example.fcc_exercise_tracker.repository.UserCrudRepository;
import com.example.fcc_exercise_tracker.util.OptionalHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ExerciseService {

    private final ExerciseCrudRepository exerciseCrudRepository;

    private final UserCrudRepository userCrudRepository;

    private final OptionalHandler optionalHandler;

    public UserExerciseDTO addExercise(long userId, ExerciseDTO exerciseDTO) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = exerciseDTO.getDate().isEmpty() ? dateFormat.format(new Date()) : exerciseDTO.getDate();

        User user = optionalHandler.getFromOptional(userCrudRepository.findById(userId));

        Exercise exercise = Exercise.builder()
                .description(exerciseDTO.getDescription())
                .duration(exerciseDTO.getDuration())
                .date(date)
                .user(user)
                .build();

        exerciseCrudRepository.save(exercise);

        return UserExerciseDTO.builder()
                .userId(user.getUserId())
                .username(user.getUsername())
                .description(exercise.getDescription())
                .duration(exercise.getDuration())
                .date(exercise.getDate())
                .build();
    }

    public FullResponseUserDTO getUserWithExercises(long userId) {
        User user = optionalHandler.getFromOptional(userCrudRepository.findById(userId));
        int count = exerciseCrudRepository.getExcerciseCount(user) == null ? 0 : exerciseCrudRepository.getExcerciseCount(user);
        List<ExerciseDTO> exerciseDTOS = new ArrayList<>();

        user.getLogs().forEach(exercise -> {
            exerciseDTOS.add(ExerciseDTO.builder()
                            .exerciseId(exercise.getExerciseId())
                            .description(exercise.getDescription())
                            .duration(exercise.getDuration())
                            .date(exercise.getDate())
                            .build());
        });

        return FullResponseUserDTO.builder()
                .userId(user.getUserId())
                .username(user.getUsername())
                .count(count)
                .logs(exerciseDTOS)
                .build();
    }

    public void deleteExercise(long exerciseId) {
        exerciseCrudRepository.deleteById(exerciseId);
    }

}

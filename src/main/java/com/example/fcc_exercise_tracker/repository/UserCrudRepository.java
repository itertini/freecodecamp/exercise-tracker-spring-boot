package com.example.fcc_exercise_tracker.repository;

import com.example.fcc_exercise_tracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCrudRepository extends JpaRepository<User, Long> {
}

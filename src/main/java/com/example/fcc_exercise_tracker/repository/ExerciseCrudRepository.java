package com.example.fcc_exercise_tracker.repository;

import com.example.fcc_exercise_tracker.entity.Exercise;
import com.example.fcc_exercise_tracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseCrudRepository extends JpaRepository<Exercise, Long> {

    @Query("SELECT count(*) FROM Exercise e WHERE e.user= :user")
    Integer getExcerciseCount(User user);
}

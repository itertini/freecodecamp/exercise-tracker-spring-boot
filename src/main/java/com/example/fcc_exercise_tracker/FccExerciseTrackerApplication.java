package com.example.fcc_exercise_tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FccExerciseTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FccExerciseTrackerApplication.class, args);
	}

}

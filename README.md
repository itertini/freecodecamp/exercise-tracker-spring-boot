# Exercise Tracker API - Spring Boot Implementation

This project is a Spring Boot implementation of the Node.js Exercise Tracker API project. You can find the original project on Exercise Tracker API on [FreeCodeCamp](https://www.freecodecamp.org/learn/back-end-development-and-apis/back-end-development-and-apis-projects/exercise-tracker).

## Created with:
Backend
- Spring Boot
- Hibernate
- MariaDB
- Lombok

Frontend
- Html
- CSS
- Bootstrap
- Vanilla JavaScript


# Project Setup

## Prerequisites

    - Java 11 or higher
    - Maven

## Installation

Clone the repository:
```
git clone https://gitlab.com/itertini/freecodecamp/exercise-tracker-spring-boot.git
cd exercise-tracker-spring-boot
```

Build the project:
```
mvn clean install
```


Start the application:
```
mvn spring-boot:run
```

Access it through the Browser:
```
firefox http://localhost:8080/index.html
```

## Endpoints

### Create a New User

**Endpoint:** `POST /api/users`

**Description:** Creates a new user with the specified username.

**Form Data:**
- `username`: The username of the new user.

**Response:**
- `username`: The username of the new user.
- `userId`: The unique ID of the new user.

**Example:**
```json
{
  "username": "john_doe",
  "userId": 1
}
```

### Get All Users

**Endpoint:** `GET /api/users`

**Description:** Retrieves a list of all users.

**Form Data:**
- `username`: An array of user objects, each containing:

**Response:**
- `username`: The username of the user.
- `userId`: The unique ID of the user.

**Example:**
```json
[
  {
    "username": "john_doe",
    "userId": 1
  },
  {
    "username": "jane_doe",
    "userId": 2
  }
]
```

### Add an Exercise

**Endpoint:**  `POST /api/users/:_id/exercises`

**Description:** Adds an exercise to the user's log

**Form Data:**
- description: The description of the exercise.
- duration: The duration of the exercise (in minutes).
- date (optional): The date of the exercise (in yyyy-mm-dd format). If not provided, the current date will be used.

**Response:**
The user object with the added exercise fields.

**Example:**
```json
{
  "username": "john_doe",
  "userId": 1,
  "description": "Running",
  "duration": 30,
  "date": "2023-06-06"
}
```

### Get User's Exercise Log

**Endpoint:**  `GET /api/users/:_id/logs`

**Description:** Retrieves the exercise log of a user.

**Response:**
The user object with a count property representing the number of exercises and a log array containing all exercises.

**Example:**
```json
{
  "username": "john_doe",
  "userId": 1,
  "count": 2,
  "log": [
    {
      "description": "Running",
      "duration": 30,
      "date": "2023-06-06"
    },
    {
      "description": "Swimming",
      "duration": 45,
      "date": "2023-06-07"
    }
  ]
}
```